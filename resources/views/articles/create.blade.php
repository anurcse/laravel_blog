@extends('pages.app')

@section('content')

    <h1>Write a New Article</h1>

    <hr/>
@include('errors.list')

    {!! Form::open(['url'=>'articles']) !!}
    @include('articles.form',['submiButtonText'=>'Add Article'])
    {!! Form::close() !!}


@stop